using MovieRama.Config;

var builder = WebApplication.CreateBuilder(args);

builder.ConfigureServices();

var app = builder.Build();

app.ConfigureApplicationPipeline();

app.Run();

public partial class Program { }
