﻿// Please see documentation at https://learn.microsoft.com/aspnet/core/client-side/bundling-and-minification
// for details on configuring this project to bundle and minify static web assets.

// Write your JavaScript code.
document.addEventListener('DOMContentLoaded', () => {
    const mainContent = document.querySelector('.main-content');

    mainContent.addEventListener('click', function (event) {
        const postElement = event.target.closest('.post');

        if (postElement == null) {
            return;
        }

        const movieId = postElement.getAttribute('data-movie-id');
        const ratingScale = parseInt(event.target.getAttribute('data-rating-scale'));
        const requestVerificationToken = document.getElementById("RequestVerificationToken").value;

        var promise = fetch('/userratings/v1/movies/' + movieId, {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
                'RequestVerificationToken': requestVerificationToken
            },
            body: JSON.stringify({
                "ratingScale": ratingScale
            }),
        });

        const likesCountElem = postElement.querySelector('.likes-count');
        const hatesCountElem = postElement.querySelector('.hates-count');

        let likesCount = parseInt(likesCountElem.textContent, 10);
        let hatesCount = parseInt(hatesCountElem.textContent, 10);

        if (event.target.classList.contains('like')) {
            event.preventDefault();

            promise.then((response) => {
                if(!response.ok) {
                    throw new Error('Something went wrong');
                }

                likesCount++;

                event.target.closest('.like-hate-action').innerHTML =
                    `You like this movie! <a href="#" class="unlike">Unlike</a>`;

                likesCountElem.textContent = likesCount;
            });
        }
        else if (event.target.classList.contains('hate')) {
            event.preventDefault();
            promise.then((response) => {
                if (!response.ok) {
                    throw new Error('Something went wrong');
                }

                hatesCount++;

                event.target.closest('.like-hate-action').innerHTML =
                    `You hate this movie! <a href="#" class="unhate">Unhate</a>`;

                hatesCountElem.textContent = hatesCount;
            })
        }
        else if (event.target.classList.contains('unlike')) {
            event.preventDefault();
            promise.then((response) => {
                if (!response.ok) {
                    throw new Error('Something went wrong');
                }

                likesCount--;

                var innerHtml = `<a href="#" class="like" data-rating-scale="5">Like</a> | <a href="#" class="hate" data-rating-scale="0">Hate</a>`;

                if (likesCount == 0 && hatesCount == 0) {
                    innerHtml = 'Be the first to vote for this movie: <a href="#" class="like" data-rating-scale="5">Like</a> | <a href="#" class="hate" data-rating-scale="0">Hate</a>';
                }

                event.target.closest('.like-hate-action').innerHTML = innerHtml;

                likesCountElem.textContent = likesCount;
            })
        }
        else if (event.target.classList.contains('unhate')) {
            event.preventDefault();
            promise.then((response) => {
                if (!response.ok) {
                    throw new Error('Something went wrong');
                }

                hatesCount--;

                var innerHtml = `<a href="#" class="like" data-rating-scale="5">Like</a> | <a href="#" class="hate" data-rating-scale="0">Hate</a>`;

                if (likesCount == 0 && hatesCount == 0) {
                    innerHtml = 'Be the first to vote for this movie: <a href="#" class="like" data-rating-scale="5">Like</a> | <a href="#" class="hate" data-rating-scale="0">Hate</a>';
                }

                event.target.closest('.like-hate-action').innerHTML = innerHtml;

                hatesCountElem.textContent = hatesCount;
            })
        }

        promise.catch(() => {
            console.error('Error rating movie');
        });
    });
});
