﻿namespace MovieRama.Web.Extensions
{
    using System.Security.Claims;

    /// <summary>
    /// 
    /// </summary>
    public static class Extensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="principal"></param>
        /// <returns></returns>
        public static Guid GetUserId(this ClaimsPrincipal principal)
        {
            if (principal == null) {
                throw new ArgumentNullException(nameof(principal));
            }

            var claim = principal.FindFirst(ClaimTypes.NameIdentifier);

            if (Guid.TryParse(claim?.Value, out var userId)) {
                return userId;
            }

            return default;
        }
    }
}
