
namespace MovieRama.Web.Pages
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Authorization;
    using Microsoft.AspNetCore.Mvc.RazorPages;
    
    using MovieRama.Web.Extensions;

    /// <summary>
    /// 
    /// </summary>
    [Authorize]
    [ValidateAntiForgeryToken]
    public class AddMovieModel : PageModel
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Services.UserMovies.IService movieService_;


        /// <summary>
        /// 
        /// </summary>
        /// <param name="movieService"></param>
        public AddMovieModel(Services.UserMovies.IService movieService)
        {
            movieService_ = movieService;
        }

        /// <summary>
        /// 
        /// </summary>
        public void OnGet()
        {
        }

        [BindProperty]
        public Services.UserMovies.Models.CreateOptions createOptions { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid) {
                return Page();
            }

            await movieService_.CreateAsync(User.GetUserId(), createOptions);

            return RedirectToPage("./Index");
        }
    }
}
