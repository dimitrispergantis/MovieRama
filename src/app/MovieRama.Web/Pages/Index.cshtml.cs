namespace MovieRama.Web.Pages
{
    using MovieRama.Web.Extensions;
    using Microsoft.AspNetCore.Mvc.RazorPages;

    /// <summary>
    /// 
    /// </summary>
    public class IndexModel : PageModel
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Services.UserRatings.IService service_;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public IndexModel(Services.UserRatings.IService service)
        {
            service_ = service;
        }

        /// <summary>
        /// 
        /// </summary>
        public string HateSort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string LikeSort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatedSort { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CurrentUsername { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<Services.UserRatings.Dtos.Movie> Movies { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        public IReadOnlyCollection<Services.UserRatings.Dtos.UserRating> UserRatings { get; private set; }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task OnGetAsync(string sortOrder, string username)
        {
            CreatedSort = string.IsNullOrEmpty(sortOrder) ? 
                Constants.MoviesSortOptions.CreatedAscending : "";
            
            LikeSort =  sortOrder == Constants.MoviesSortOptions.LikesAscending ?
                Constants.MoviesSortOptions.LikesDescending : 
                Constants.MoviesSortOptions.LikesAscending;
            
            HateSort = sortOrder == Constants.MoviesSortOptions.HatesAscending ?
                Constants.MoviesSortOptions.HatesDescending : 
                Constants.MoviesSortOptions.HatesAscending;

            CurrentUsername = username;

            Movies = await service_.GetMoviesAsync(
                new Services.UserRatings.Models.GetMoviesOptions {
                    Username = username,
                    SortOrder = sortOrder
                });

            UserRatings = await service_.GetUserRatingsAsync(
                User.GetUserId());
        }
    }
}
