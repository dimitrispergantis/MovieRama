﻿namespace MovieRama.Web.Controllers
{
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.AspNetCore.Authorization;
    
    using MovieRama.Web.Extensions;

    [Authorize]
    [ApiController]
    [Route("userratings/v1")]
    public class UserRatingsController : ControllerBase
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Services.UserRatings.IService service_;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="service"></param>
        public UserRatingsController(
            Services.UserRatings.IService service)
        {
            service_ = service;
        }

        [ValidateAntiForgeryToken]
        [HttpPost("movies/{movieId}")]
        public async Task<IActionResult> RateMovieAsync(
            Guid movieId, [FromBody] Services.UserRatings.Models.RateOptions options)
        {
            await service_.RateMovieAsync(
                User.GetUserId(), movieId, options);

            return NoContent();
        }
    }
}
