﻿namespace MovieRama.Ratings
{
    using System.Threading.Tasks;

    /// <summary>
    /// 
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="movieId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<Model.Core.Rating> RateAsync(Guid userId, Guid movieId,
            Models.RateOptions options);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        IQueryable<Model.Core.Rating> Search(Models.SearchOptions options);
    }
}
