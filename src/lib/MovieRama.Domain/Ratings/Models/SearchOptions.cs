﻿namespace MovieRama.Ratings.Models
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class SearchOptions
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid? MovieId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid? UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool TrackResults { get; set; }
    }
}
