﻿namespace MovieRama.Ratings
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;

    using MovieRama.Data;
    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class DefaultRatingService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ApplicationDbContext context_;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="context"></param>
        public DefaultRatingService(ApplicationDbContext context)
        {
            context_ = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IQueryable<Model.Core.Rating> Search(
            Models.SearchOptions options)
        {
            if (options == null) {
                throw new DomainException($"Null {nameof(options)}");
            }

            var query = context_.Ratings.AsQueryable();

            if (!options.TrackResults) {
                query = query.AsNoTracking();
            }

            if (options.UserId != null) {
                query = query.Where(r => r.UserId == options.UserId.Value);
            }

            if (options.MovieId != null) {
                query = query.Where(r => r.MovieId == options.MovieId.Value);
            }

            query = query.OrderByDescending(r => r.AuditInfo.Created);

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="movieId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<Model.Core.Rating> RateAsync(Guid userId, Guid movieId,
            Models.RateOptions options)
        {
            if (options == null) {
                throw new DomainException($"Null {nameof(options)}");
            }

            if (options.RatingScale != null &&
              !Enum.IsDefined(options.RatingScale.Value)) {
                throw new DomainException($"Invalid {options.RatingScale}");
            }

            var user = await context_.Users.FindAsync(userId);

            if (user == null) {
                throw new DomainException($"{nameof(user)} not found");
            }

            var movie = await context_.Movies.FindAsync(movieId);

            if (movie == null) {
                throw new DomainException($"{nameof(movie)} not found");
            }

            if (movie.UserId == user.Id) {
                throw new DomainException($"Rating is not permitted");
            }

            var rating = await context_.Ratings
                .Where(o => o.UserId == userId)
                .Where(o => o.MovieId == movieId)
                .SingleOrDefaultAsync();

            if (rating != null) {
                rating.RatingScale = options.RatingScale != null 
                    ? options.RatingScale.Value : null;
            } else {
                if (options.RatingScale == null) {
                    throw new DomainException($"Null {nameof(options.RatingScale)}");
                }

                rating = new Model.Core.Rating {
                    UserId = user.Id,
                    MovieId = movie.Id,
                    RatingScale = options.RatingScale
                };

                rating.AuditInfo.CreatedBy = user.UserName!;

                await context_.Ratings.AddAsync(rating);
            }

            await context_.SaveChangesAsync();

            return rating;
        }
    }
}
