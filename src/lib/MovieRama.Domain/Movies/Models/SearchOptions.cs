﻿namespace MovieRama.Movies.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class SearchOptions
    {
        /// <summary>
        /// 
        /// </summary>
        public string? Username { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public bool TrackResults { get; set; }
    }
}
