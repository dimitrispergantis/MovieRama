﻿namespace MovieRama.Movies
{
    /// <summary>
    /// 
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        IQueryable<Model.Core.Movie> Search(Models.SearchOptions options);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<Model.Core.Movie> CreateAsync(Guid userId, Models.CreateOptions options);
    }
}
