﻿namespace MovieRama.Movies
{
    using System;
    using System.Linq;
    using System.Threading.Tasks;

    using Microsoft.EntityFrameworkCore;
    
    using MovieRama.Data;
    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    internal class DefaultMovieService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly ApplicationDbContext context_;

        /// <summary>
        /// 
        /// </summary>
        public DefaultMovieService(ApplicationDbContext context)
        {
            context_ = context;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        public IQueryable<Model.Core.Movie> Search(
            Models.SearchOptions options)
        {
            if (options == null) {
                throw new DomainException($"Null {nameof(options)}");
            }

            var query = context_.Movies.AsQueryable();

            if (!options.TrackResults) {
                query = query.AsNoTracking();
            }

            if (!string.IsNullOrWhiteSpace(options.Username)) {
                query = query.Where(o => o.AuditInfo.CreatedBy.Equals(options.Username));
            }

            query = query.OrderByDescending(m => m.AuditInfo.Created);

            return query;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        public async Task<Model.Core.Movie> CreateAsync(
            Guid userId, Models.CreateOptions options)
        {
            if (options == null) {
                throw new DomainException($"Null {nameof(options)}");
            }

            if (string.IsNullOrWhiteSpace(options.Title)) {
                throw new DomainException($"Null {nameof(options.Title)}");
            }

            if (string.IsNullOrWhiteSpace(options.Description)) {
                throw new DomainException($"Null {nameof(options.Description)}");
            }

            var user = await context_.Users.FindAsync(userId);

            if (user == null) {
                throw new DomainException($"{nameof(user)} not found");
            }

            var movieExists = await context_.Movies
                .Where(m => m.Title == options.Title)
                .AnyAsync();

            if (movieExists) {
                throw new DomainException($"Movie with title: {options.Title} already exists");
            }

            var movie = new Model.Core.Movie {
                UserId = userId,
                Title = options.Title,
                Description = options.Description,
            };

            movie.AuditInfo.CreatedBy = user.UserName;

            await context_.AddAsync(movie);

            await context_.SaveChangesAsync();

            return movie;
        }
    }
}
