﻿namespace MovieRama.Model.Complex
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class AuditInfo
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DateTimeOffset Created { get; set; }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public AuditInfo()
        {
            Created = DateTimeOffset.Now;
        }
    }
}
