﻿namespace MovieRama.Model.Core
{
    using System;

    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class Movie : IEntity
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        #endregion

        #region Navigation Properties

        /// <summary>
        /// 
        /// </summary>
        public virtual Accounts.User User { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual List<Rating> Ratings { get; set; }

        #endregion

        #region Complex

        /// <summary>
        /// 
        /// </summary>
        public Complex.AuditInfo AuditInfo { get; set; }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public Movie()
        {
            Id = Guid.NewGuid();
            AuditInfo = new Complex.AuditInfo();
        }
    }
}
