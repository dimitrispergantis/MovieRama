﻿namespace MovieRama.Model.Core
{
    using System;
    
    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class Rating : IEntity
    {
        #region Properties

        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid MovieId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Constants.RatingScale? RatingScale { get; set; }

        #endregion

        #region Navigation Properties

        /// <summary>
        /// 
        /// </summary>
        public virtual Movie Movie { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public virtual Accounts.User User { get; set; }

        #endregion

        #region Complex

        /// <summary>
        /// 
        /// </summary>
        public Complex.AuditInfo AuditInfo { get; set; }

        #endregion

        /// <summary>
        /// 
        /// </summary>
        public Rating()
        {
            Id = Guid.NewGuid();
            AuditInfo = new Complex.AuditInfo();
        }
    }
}
