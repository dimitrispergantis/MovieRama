﻿namespace MovieRama.Model.Accounts
{
    using System;

    using Microsoft.AspNetCore.Identity;

    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class UserRole : IdentityUserRole<Guid>, IEntity
    {

    }
}
