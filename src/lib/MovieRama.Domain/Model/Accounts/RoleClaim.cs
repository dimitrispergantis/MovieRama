﻿namespace MovieRama.Model.Accounts
{
    using System;

    using Microsoft.AspNetCore.Identity;

    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class RoleClaim: IdentityRoleClaim<Guid>, IEntity
    {

    }
}
