﻿namespace MovieRama.Model.Accounts
{
    using System;
    
    using Microsoft.AspNetCore.Identity;

    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class Role : IdentityRole<Guid>, IEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public Role() : base()
        {
            Id = Guid.NewGuid();
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }
    }
}
