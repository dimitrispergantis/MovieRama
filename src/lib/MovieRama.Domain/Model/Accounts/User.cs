﻿namespace MovieRama.Model.Accounts
{
    using System;
    
    using Microsoft.AspNetCore.Identity;

    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class User : IdentityUser<Guid>, IEntity
    {
        /// <summary>
        /// 
        /// </summary>
        public User() : base()
        {
            Id = Guid.NewGuid();
            ConcurrencyStamp = Guid.NewGuid().ToString();
        }
    }
}
