﻿namespace MovieRama.Constants
{
    /// <summary>
    /// 
    /// </summary>
    public enum RatingScale: byte
    {
        /// <summary>
        /// 
        /// </summary>
        ZeroStars = 0,

        /// <summary>
        /// 
        /// </summary>
        FiveStars = 5
    }
}
