﻿namespace MovieRama.Constants
{
    /// <summary>
    /// 
    /// </summary>
    public static class MoviesSortOptions
    {
        /// <summary>
        /// 
        /// </summary>
        public const string HatesAscending = "hates";

        /// <summary>
        /// 
        /// </summary>
        public const string LikesAscending = "likes";

        /// <summary>
        /// 
        /// </summary>
        public const string CreatedAscending = "created";

        /// <summary>
        /// 
        /// </summary>
        public const string LikesDescending = "likes_desc";

        /// <summary>
        /// 
        /// </summary>
        public const string HatesDescending = "hates_desc";
    }
}
