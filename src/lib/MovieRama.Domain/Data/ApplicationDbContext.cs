﻿namespace MovieRama.Data
{
    using Microsoft.EntityFrameworkCore;
    using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
    using Microsoft.AspNetCore.DataProtection.EntityFrameworkCore;

    /// <summary>
    /// 
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<Model.Accounts.User, Model.Accounts.Role, Guid,
        Model.Accounts.UserClaim, Model.Accounts.UserRole, Model.Accounts.UserLogin, Model.Accounts.RoleClaim, Model.Accounts.UserToken>,
        IDataProtectionKeyContext
    {
        /// <summary>
        /// 
        /// </summary>
        public DbSet<Model.Core.Movie> Movies { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<Model.Core.Rating> Ratings { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public DbSet<DataProtectionKey> DataProtectionKeys { get; set; } = null!;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            AddDboSchemaConfiguration(builder);
            AddCoreSchemaConfiguration(builder);
            AddAccountsSchemaConfiguration(builder);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void AddDboSchemaConfiguration(
            ModelBuilder builder)
        {
            const string schema = "dbo";

            if (builder == null) {
                throw new NullReferenceException(nameof(builder));
            }

            if (string.IsNullOrWhiteSpace(schema)) {
                throw new NullReferenceException(nameof(schema));
            }

            builder.Entity<DataProtectionKey>().ToTable("DataProtectionKey", "dbo");

            builder.Entity<DataProtectionKey>().HasKey(d => d.Id);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        public void AddCoreSchemaConfiguration(
            ModelBuilder builder)
        {
            const string schema = "core";

            if (builder == null) {
                throw new NullReferenceException(nameof(builder));
            }

            if (string.IsNullOrWhiteSpace(schema)) {
                throw new NullReferenceException(nameof(schema));
            }

            builder.Entity<Model.Core.Movie>().ToTable("Movie", schema);
            builder.Entity<Model.Core.Rating>().ToTable("Rating", schema);

            builder.Entity<Model.Core.Movie>().HasKey(m => m.Id);
            builder.Entity<Model.Core.Rating>().HasKey(m => m.Id);

            builder.Entity<Model.Core.Movie>().HasOne(m => m.User).WithMany();
            builder.Entity<Model.Core.Movie>().HasMany(m => m.Ratings).WithOne();

            builder.Entity<Model.Core.Rating>().HasOne(m => m.User).WithMany();
            builder.Entity<Model.Core.Rating>().HasOne(m => m.Movie).WithMany()
                .HasForeignKey(r => r.Id);

            builder.Entity<Model.Core.Movie>().ComplexProperty(m => m.AuditInfo)
                .Property(x => x.Created).HasColumnName("Created");
            builder.Entity<Model.Core.Movie>().ComplexProperty(m => m.AuditInfo)
                .Property(x => x.CreatedBy).HasColumnName("CreatedBy");

            builder.Entity<Model.Core.Rating>().ComplexProperty(m => m.AuditInfo)
                .Property(x => x.Created).HasColumnName("Created");
            builder.Entity<Model.Core.Rating>().ComplexProperty(m => m.AuditInfo)
               .Property(x => x.CreatedBy).HasColumnName("CreatedBy");
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <param name="schema"></param>
        public void AddAccountsSchemaConfiguration(
            ModelBuilder builder)
        {
            const string schema = "accounts";

            if (builder == null) {
                throw new NullReferenceException(nameof(builder));
            }

            if (string.IsNullOrWhiteSpace(schema)) {
                throw new NullReferenceException(nameof(schema));
            }

            builder.Entity<Model.Accounts.Role>().ToTable("Role", schema);
            builder.Entity<Model.Accounts.User>().ToTable("User", schema);
            builder.Entity<Model.Accounts.UserRole>().ToTable("UserRole", schema);
            builder.Entity<Model.Accounts.RoleClaim>().ToTable("RoleClaim", schema);
            builder.Entity<Model.Accounts.UserClaim>().ToTable("UserClaim", schema);
            builder.Entity<Model.Accounts.UserLogin>().ToTable("UserLogin", schema);
            builder.Entity<Model.Accounts.UserToken>().ToTable("UserToken", schema);
        }
    }
}
