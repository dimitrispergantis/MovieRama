﻿namespace MovieRama.Services.UserMovies
{
    /// <summary>
    /// 
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="options"></param>
        /// <returns></returns>
        Task<Dtos.Movie> CreateAsync(Guid userId, Models.CreateOptions options);
    }
}
