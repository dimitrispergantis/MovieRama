﻿namespace MovieRama.Services.UserMovies
{
    using System;
    using System.Threading.Tasks;
    
    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public class DefaultUserMovieService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Movies.IService movieService_;

        /// <summary>
        /// 
        /// </summary>
        public DefaultUserMovieService(Movies.IService movieService)
        {
            movieService_ = movieService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        /// <exception cref="DomainException"></exception>
        public async Task<Dtos.Movie> CreateAsync(
            Guid userId, Models.CreateOptions options)
        {
            if (options == null) {
                throw new DomainException($"Null {nameof(options)}");
            }

            var movie = await movieService_.CreateAsync(userId,
                new Movies.Models.CreateOptions {
                    Title = options.Title,
                    Description = options.Description
                });

            return new Dtos.Movie {
                MovieId = movie.Id
            };
        }
    }
}
