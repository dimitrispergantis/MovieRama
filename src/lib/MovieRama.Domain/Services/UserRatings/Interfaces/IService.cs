﻿namespace MovieRama.Services.UserRatings
{
    /// <summary>
    /// 
    /// </summary>
    public interface IService
    {
        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        Task<IReadOnlyCollection<Dtos.Movie>> GetMoviesAsync(
            Models.GetMoviesOptions options);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        Task<IReadOnlyCollection<Dtos.UserRating>> GetUserRatingsAsync(Guid userId);

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="movieId"></param>
        /// <param name=""></param>
        /// <returns></returns>
        Task<Dtos.Rating> RateMovieAsync(Guid userId, Guid movieId, Models.RateOptions options);
    }
}
