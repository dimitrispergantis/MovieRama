﻿namespace MovieRama.Services.UserRatings.Dtos
{
    /// <summary>
    /// 
    /// </summary>
    public class Movie
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid Id { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Guid UserId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Title { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int FiveStars { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public int ZeroStars { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public TimeSpan TimeElapsed { get; set; }
    }
}
