﻿namespace MovieRama.Services.UserRatings.Dtos
{
    using System;

    /// <summary>
    /// 
    /// </summary>
    public class UserRating
    {
        /// <summary>
        /// 
        /// </summary>
        public Guid MovieId { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public Constants.RatingScale? RatingScale { get; set; }
    }
}
