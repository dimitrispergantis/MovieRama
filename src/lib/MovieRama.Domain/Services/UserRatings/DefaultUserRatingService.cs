﻿
namespace MovieRama.Services.UserRatings
{
    using System.Linq.Expressions;

    using Microsoft.EntityFrameworkCore;

    using AutoMapper;
    using AutoMapper.QueryableExtensions;

    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    internal class DefaultUserRatingService : IService
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IMapper mapper_;

        /// <summary>
        /// 
        /// </summary>
        private readonly Movies.IService moviesService_;

        /// <summary>
        /// 
        /// </summary>
        private readonly Ratings.IService ratingsService_;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="mapper"></param>
        /// <param name="ratingsService"></param>
        /// <param name="moviesService"></param>
        public DefaultUserRatingService(
            IMapper mapper,
            Ratings.IService ratingsService,
            Movies.IService moviesService)
        {
            mapper_ = mapper;
            moviesService_ = moviesService;
            ratingsService_ = ratingsService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task<IReadOnlyCollection<Dtos.Movie>> GetMoviesAsync(
            Models.GetMoviesOptions options)
        {
            if (options == null) {
                throw new DomainException($"Null {nameof(options)}");
            }

            var query = moviesService_.Search(
                new Movies.Models.SearchOptions {
                    Username = options.Username
                });

            Expression<Func<Model.Core.Movie, int>> countZeroStars =
                m => m.Ratings.Where(r => r.RatingScale == Constants.RatingScale.ZeroStars).Count();
            Expression<Func<Model.Core.Movie, int>> countFiveStars =
                m => m.Ratings.Where(r => r.RatingScale == Constants.RatingScale.FiveStars).Count();

            query = options.SortOrder switch {
                Constants.MoviesSortOptions.LikesAscending =>
                    query.OrderBy(countFiveStars),
                Constants.MoviesSortOptions.LikesDescending =>
                    query.OrderByDescending(countFiveStars),
                Constants.MoviesSortOptions.HatesAscending =>
                    query.OrderBy(countZeroStars),
                Constants.MoviesSortOptions.HatesDescending =>
                    query.OrderByDescending(countZeroStars),
                Constants.MoviesSortOptions.CreatedAscending =>
                    query.OrderBy(m => m.AuditInfo.Created),
                _ => query.OrderByDescending(m => m.AuditInfo.Created)
            };

            return await query.ProjectTo<Dtos.Movie>(
                    mapper_.ConfigurationProvider)
                 .ToListAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="movieId"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        /// <exception cref="DomainException"></exception>
        public async Task<Dtos.Rating> RateMovieAsync(
            Guid userId, Guid movieId, Models.RateOptions options)
        {
            if (options == null) {
                throw new DomainException($"Null {nameof(options)}");
            }

            var rating = await ratingsService_.RateAsync(
                userId, movieId, new Ratings.Models.RateOptions {
                    RatingScale = options.RatingScale
                });

            return new Dtos.Rating {
                RatingId = rating.Id
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="userId"></param>
        /// <returns></returns>
        public async Task<IReadOnlyCollection<Dtos.UserRating>> GetUserRatingsAsync(
            Guid userId)
        {
            return await ratingsService_.Search(
                new Ratings.Models.SearchOptions {
                    UserId = userId
                })
                .ProjectTo<Dtos.UserRating>(mapper_.ConfigurationProvider)
                .ToListAsync();
        }
    }
}
