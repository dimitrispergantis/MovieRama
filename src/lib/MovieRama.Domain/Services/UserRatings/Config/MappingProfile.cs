﻿namespace MovieRama.Services.UserRatings.Config
{
    using AutoMapper;

    /// <summary>
    /// 
    /// </summary>
    public class MappingProfile : Profile
    {
        /// <summary>
        /// 
        /// </summary>
        public MappingProfile()
        {
            CreateMap<Model.Core.Movie, Dtos.Movie>()
                .ForMember(dst => dst.CreatedBy,
                    opt => opt.MapFrom(s => s.AuditInfo.CreatedBy))
                .ForMember(dst => dst.TimeElapsed, 
                    opt => opt.MapFrom(s => DateTimeOffset.Now - s.AuditInfo.Created))
                .ForMember(dst => dst.ZeroStars, 
                    opt => opt.MapFrom(s => s.Ratings.Where(r => r.RatingScale == Constants.RatingScale.ZeroStars).Count()))
                .ForMember(dst => dst.FiveStars, 
                    opt => opt.MapFrom(s => s.Ratings.Where(r => r.RatingScale == Constants.RatingScale.FiveStars).Count()));

            CreateMap<Model.Core.Rating, Dtos.UserRating>();
        }
    }
}
