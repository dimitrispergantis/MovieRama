﻿namespace MovieRama.Services.UserRatings.Models
{
    /// <summary>
    /// 
    /// </summary>
    public class GetMoviesOptions
    {
        /// <summary>
        /// 
        /// </summary>
        public string? Username { get; set; }

        /// <summary>
        /// 
        /// </summary>
        public string? SortOrder { get; set; }
    }
}
