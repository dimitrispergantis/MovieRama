﻿namespace MovieRama.SeedWork
{
    using System;
    using System.Net;

    using Microsoft.AspNetCore.Http;

    /// <summary>
    /// 
    /// </summary>
    public class ExceptionHandler : Microsoft.AspNetCore.Diagnostics.IExceptionHandler
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IProblemDetailsService problemDetailsService_;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="problemDetailsService"></param>
        public ExceptionHandler(IProblemDetailsService problemDetailsService)
        {
            problemDetailsService_ = problemDetailsService;
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="httpContext"></param>
        /// <param name="exception"></param>
        /// <param name="cancellationToken"></param>
        /// <returns></returns>
        public async ValueTask<bool> TryHandleAsync(
            HttpContext httpContext, Exception exception, CancellationToken cancellationToken)
        {
            httpContext.Response.StatusCode = (int)HttpStatusCode.BadRequest;

            return await problemDetailsService_.TryWriteAsync(
                new ProblemDetailsContext {
                    Exception = exception,
                    HttpContext = httpContext,
                    ProblemDetails =
                    {
                        Title = "An error occurred",
                        Detail = exception.Message,
                        Type = exception.GetType().Name,
                    }
                });
        }
    }
}
