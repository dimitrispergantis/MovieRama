﻿namespace MovieRama.Config
{
    using Microsoft.AspNetCore.Builder;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.AspNetCore.DataProtection;
    using Microsoft.Extensions.DependencyInjection;
    
    using MovieRama.SeedWork;

    /// <summary>
    /// 
    /// </summary>
    public static class ServicesProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        public static void ConfigureServices(
            this WebApplicationBuilder builder)
        {
            builder.Services.AddIdentity();
            builder.Services.AddRazorPages();
            builder.Services.AddCustomServices(); 
            builder.Services.AddProblemDetails(); 
            builder.Services.AddDbContext(builder.Configuration);
            builder.Services.AddExceptionHandler<ExceptionHandler>();
            builder.Services.AddAutoMapper(AppDomain.CurrentDomain.GetAssemblies());
            builder.Services.AddDataProtection().PersistKeysToDbContext<Data.ApplicationDbContext>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        /// <exception cref="InvalidOperationException"></exception>
        private static void AddDbContext(
            this IServiceCollection services, IConfiguration configuration)
        {
            var connectionString = configuration.GetConnectionString("DefaultConnection")
                ?? throw new InvalidOperationException("Connection string 'ConnectionString' not found.");

            services.AddDbContext<Data.ApplicationDbContext>(options =>
                options.UseLazyLoadingProxies().UseSqlServer(connectionString));
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="builder"></param>
        private static void AddIdentity(
            this IServiceCollection services)
        {
            services.AddDefaultIdentity<Model.Accounts.User>(
                    options => options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<Data.ApplicationDbContext>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="services"></param>
        private static void AddCustomServices(
            this IServiceCollection services)
        {
            services.AddScoped<Movies.IService, Movies.DefaultMovieService>();
            services.AddScoped<Ratings.IService, Ratings.DefaultRatingService>();
            services.AddScoped<Services.UserMovies.IService, Services.UserMovies.DefaultUserMovieService>();
            services.AddScoped<Services.UserRatings.IService, Services.UserRatings.DefaultUserRatingService>();
        }
    }
}
