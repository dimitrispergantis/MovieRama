﻿namespace MovieRama.Config
{
    using Microsoft.AspNetCore.Builder;

    /// <summary>
    /// 
    /// </summary>
    public static class ApplicationProvider
    {
        /// <summary>
        /// 
        /// </summary>
        /// <param name="app"></param>
        public static void ConfigureApplicationPipeline(
            this WebApplication app)
        {
            app.UseExceptionHandler();
            app.UseHsts();
            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseRouting();
            app.UseAuthorization();
            app.MapRazorPages();
            app.MapControllers();
        }
    }
}
