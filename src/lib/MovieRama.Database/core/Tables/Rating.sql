﻿CREATE TABLE [core].[Rating]
(
	[Id]				UNIQUEIDENTIFIER 		NOT	NULL,
	[UserId]			UNIQUEIDENTIFIER		NOT	NULL,
	[MovieId]			UNIQUEIDENTIFIER		NOT	NULL,
	[RatingScale]		TINYINT					NULL,
	[Created]			DATETIMEOFFSET (4)		NOT	NULL	CONSTRAINT [DF_core_Rating_Created] DEFAULT (sysdatetimeoffset()),
	[CreatedBy]			NVARCHAR (100)			NULL		CONSTRAINT [DF_core_Rating_CreatedBy] DEFAULT(suser_sname())
)
GO

ALTER TABLE [core].[Rating]
	ADD CONSTRAINT [PK_Rating]
		PRIMARY KEY NONCLUSTERED ([Id])
GO

ALTER TABLE [core].[Rating]
	ADD CONSTRAINT [FK_core_Rating_User]
		FOREIGN KEY ([UserId])
			REFERENCES [accounts].[User] ([Id]);
GO

ALTER TABLE [core].[Rating]
	ADD CONSTRAINT [FK_core_Rating_Movie]
		FOREIGN KEY ([MovieId])
			REFERENCES [core].[Movie] ([Id]);
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_core_Rating_UserMovie]
	ON [core].[Rating] ([UserId], [MovieId])
		INCLUDE ([RatingScale])
GO

CREATE CLUSTERED INDEX [IX_core_Rating]
    ON [core].[Rating] ([Created] DESC);
GO

