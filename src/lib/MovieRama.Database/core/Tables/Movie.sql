﻿CREATE TABLE [core].[Movie]
(
	[Id]			UNIQUEIDENTIFIER 	NOT	NULL,
	[Title]	 		NVARCHAR (100)		NOT	NULL,
	[Description]	NVARCHAR (2048)		NOT	NULL,
	[UserId]		UNIQUEIDENTIFIER	NOT	NULL,
	[Created]		DATETIMEOFFSET (4)	NOT	NULL	CONSTRAINT [DF_core_Movie_Created] DEFAULT (sysdatetimeoffset()),
	[CreatedBy]		NVARCHAR (100)		NULL		CONSTRAINT [DF_core_Movie_CreatedBy] DEFAULT(suser_sname())
);
GO

ALTER TABLE [core].[Movie]
	ADD CONSTRAINT [PK_Movie]
		PRIMARY KEY NONCLUSTERED ([Id])
GO

ALTER TABLE [core].[Movie]
	ADD CONSTRAINT [FK_core_Movie_User]
		FOREIGN KEY ([UserId])
			REFERENCES [accounts].[User] ([Id]);
GO

CREATE UNIQUE NONCLUSTERED INDEX [IX_core_Movie_Title]
	ON [core].[Movie] ([Title])
GO

CREATE NONCLUSTERED INDEX [IX_core_Movie_User]
	ON [core].[Movie] ([UserId])
GO

CREATE CLUSTERED INDEX [IX_core_Movie]
    ON [core].[Movie] ([Created] DESC);
GO
