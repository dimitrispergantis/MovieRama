﻿CREATE TABLE [dbo].[DataProtectionKey]
(
	[Id]			INT 				NOT	NULL 	IDENTITY(1, 1),
	[Xml]			TEXT				NOT NULL,
	[FriendlyName]	NVARCHAR (40)		NULL,
)
GO

ALTER TABLE [dbo].[DataProtectionKey]
	ADD CONSTRAINT [PK_DataProtectionKey]
		 PRIMARY KEY CLUSTERED ([Id])
GO
