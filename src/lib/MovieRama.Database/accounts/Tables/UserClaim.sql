﻿CREATE TABLE [accounts].[UserClaim] (
	[Id]			INT					NOT NULL	IDENTITY(1, 1),
	[UserId]		UNIQUEIDENTIFIER	NOT NULL,
	[ClaimType]		NVARCHAR (MAX)		NULL,
	[ClaimValue]	NVARCHAR (MAX)		NULL
);
GO
ALTER TABLE [accounts].[UserClaim]
	ADD CONSTRAINT [PK_accounts_UserClaim]
		PRIMARY KEY CLUSTERED ([Id]);
GO
ALTER TABLE [accounts].[UserClaim]
	ADD CONSTRAINT [FK_accounts_UserClaim_User]
		FOREIGN KEY ([UserId])
			REFERENCES [accounts].[User] ([Id]);
GO
CREATE NONCLUSTERED INDEX [IX_accounts_UserClaim_UserId]
	ON [accounts].[UserClaim] ([UserId]);