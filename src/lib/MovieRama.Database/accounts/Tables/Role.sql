﻿CREATE TABLE [accounts].[Role] (
	[Id]				UNIQUEIDENTIFIER	NOT NULL,
	[Name]				NVARCHAR (256)		NOT NULL,
	[NormalizedName]	NVARCHAR (256)		NOT NULL,
	[ConcurrencyStamp]	NVARCHAR (MAX)		NOT NULL
);
GO
ALTER TABLE [accounts].[Role]
	ADD CONSTRAINT [PK_accounts_Role]
		PRIMARY KEY CLUSTERED ([Id]);
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_accounts_Role_Name]
	ON [accounts].[Role] ([Name])
		INCLUDE ([Id]);