﻿CREATE TABLE [accounts].[UserLogin] (
	[UserId]				UNIQUEIDENTIFIER	NOT NULL,
	[ProviderKey]			NVARCHAR (128)		NOT NULL,
	[ProviderDisplayName]	NVARCHAR (MAX)		NULL,
	[LoginProvider]			NVARCHAR (128)		NOT NULL
);
GO
ALTER TABLE [accounts].[UserLogin]
	ADD CONSTRAINT [PK_accounts_UserLogin]
		PRIMARY KEY CLUSTERED ([LoginProvider], [ProviderKey]);
GO
ALTER TABLE [accounts].[UserLogin]
	ADD CONSTRAINT [FK_accounts_UserLogin_User]
		FOREIGN KEY ([UserId])
			REFERENCES [accounts].[User] ([Id]);
GO
CREATE NONCLUSTERED INDEX [IX_accounts_UserLogin_UserId]
	ON [accounts].[UserLogin] ([UserId]);