﻿CREATE TABLE [accounts].[RoleClaim] (
	[Id]			INT					NOT NULL	IDENTITY(1, 1),
	[RoleId]		UNIQUEIDENTIFIER	NOT NULL,
	[ClaimType]		NVARCHAR (MAX)		NULL,
	[ClaimValue]	NVARCHAR (MAX)		NULL
);
GO
ALTER TABLE [accounts].[RoleClaim]
	ADD CONSTRAINT [PK_accounts_RoleClaim]
		PRIMARY KEY CLUSTERED ([Id]);
GO
ALTER TABLE [accounts].[RoleClaim]
	ADD CONSTRAINT [FK_accounts_RoleClaim_Role]
		FOREIGN KEY ([RoleId])
			REFERENCES [accounts].[Role] ([Id]);
GO
CREATE NONCLUSTERED INDEX [IX_accounts_RoleClaim_RoleId]
	ON [accounts].[RoleClaim] ([RoleId]);