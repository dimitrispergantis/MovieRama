﻿CREATE TABLE [accounts].[UserToken] (
	[UserId]		UNIQUEIDENTIFIER	NOT NULL,
	[LoginProvider]	NVARCHAR (128)		NOT NULL,
	[Name]			NVARCHAR (128)		NOT NULL,
	[Value]			NVARCHAR (MAX)		NULL,
);
GO
ALTER TABLE [accounts].[UserToken]
	ADD CONSTRAINT [PK_accounts_UserToken]
		PRIMARY KEY CLUSTERED ([UserId], [LoginProvider], [Name]);
GO
ALTER TABLE [accounts].[UserToken]
	ADD CONSTRAINT [FK_accounts_UserToken_User]
		FOREIGN KEY ([UserId])
			REFERENCES [accounts].[User] ([Id]);