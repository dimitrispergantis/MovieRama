﻿CREATE TABLE [accounts].[User] (
	[Id]					UNIQUEIDENTIFIER	NOT NULL,
	[UserName]				NVARCHAR (256)		NOT NULL,
	[NormalizedUserName]	NVARCHAR (256)		NOT NULL,
	[PasswordHash]			NVARCHAR (MAX)		NULL,
	[SecurityStamp]			NVARCHAR (MAX)		NULL,
	[Email]					NVARCHAR (256)		NULL,
	[NormalizedEmail]		NVARCHAR (256)		NULL,
	[EmailConfirmed]		BIT					NOT NULL,
	[PhoneNumber]			NVARCHAR (256)		NULL,
	[PhoneNumberConfirmed]	BIT					NOT NULL,
	[TwoFactorEnabled]		BIT					NOT NULL,
	[LockoutEnd]			DATETIMEOFFSET		NULL,
	[LockoutEnabled]		BIT					NOT NULL,
	[AccessFailedCount]		INT					NOT NULL,
	[ConcurrencyStamp]		NVARCHAR (MAX)		NOT NULL
);
GO
ALTER TABLE [accounts].[User]
	ADD CONSTRAINT [PK_accounts_User]
		PRIMARY KEY CLUSTERED ([Id]);
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_accounts_User_UserName]
	ON [accounts].[User] ([UserName]);
GO
CREATE UNIQUE NONCLUSTERED INDEX [IX_accounts_User_NormalizedUserName]
	ON [accounts].[User] ([NormalizedUserName]);
