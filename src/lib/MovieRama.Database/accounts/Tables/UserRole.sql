﻿CREATE TABLE [accounts].[UserRole] (
	[UserId]	UNIQUEIDENTIFIER	NOT NULL,
	[RoleId]	UNIQUEIDENTIFIER	NOT NULL
);
GO
ALTER TABLE [accounts].[UserRole]
	ADD CONSTRAINT [PK_accounts_UserRole]
		PRIMARY KEY CLUSTERED ([UserId], [RoleId]);
GO
ALTER TABLE [accounts].[UserRole]
	ADD CONSTRAINT [FK_accounts_UserRole_User]
		FOREIGN KEY ([UserId])
			REFERENCES [accounts].[User] ([Id]);
GO
ALTER TABLE [accounts].[UserRole]
	ADD CONSTRAINT [FK_accounts_UserRole_Role]
		FOREIGN KEY ([RoleId])
			REFERENCES [accounts].[Role] ([Id]);
GO
CREATE NONCLUSTERED INDEX [IX_accounts_UserRole_RoleId]
	ON [accounts].[UserRole] ([RoleId])
		INCLUDE ([UserId]);