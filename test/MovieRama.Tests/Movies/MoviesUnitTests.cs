namespace MovieRama.Tests.Movies
{
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Mvc.Testing;
    using Microsoft.Extensions.DependencyInjection;

    using MovieRama.Data;
    using MovieRama.SeedWork;
    using MovieRama.Model.Accounts;

    /// <summary>
    /// 
    /// </summary>
    public class MoviesUnitTests : 
        IClassFixture<WebApplicationFactory<Program>>, IAsyncLifetime
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly Guid userId_ = Guid.NewGuid();

        /// <summary>
        /// 
        /// </summary>
        private readonly ApplicationDbContext context_;

        /// <summary>
        /// 
        /// </summary>
        private readonly MovieRama.Movies.IService service_;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="factory"></param>
        public MoviesUnitTests(WebApplicationFactory<Program> factory)
        {
            var scope = factory.Services
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();

            context_ = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            service_ = scope.ServiceProvider.GetRequiredService<MovieRama.Movies.IService>();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task InitializeAsync()
        {
            var user = new User {
                Id = userId_,
                Email = $"{userId_}@foobar.com",
                UserName = $"{userId_}@foobar.com",
                ConcurrencyStamp = $"{Guid.NewGuid()}",
                NormalizedUserName = $"{userId_}@foobar.com".ToUpper()
            };

            await context_.AddAsync(user);
            await context_.SaveChangesAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task DisposeAsync()
        {
            var user = await context_.Users.FindAsync(userId_);

            context_.Users.Remove(user!);

            await context_.SaveChangesAsync();
        }

        [Fact]
        public async Task Should_fail_without_null_options()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => service_.CreateAsync(userId_, default));
        }

        [Fact]
        public async Task Should_fail_without_a_title()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => service_.CreateAsync(userId_,
                    new MovieRama.Movies.Models.CreateOptions {
                        Description = "foobar"
                    }));
        }

        [Fact]
        public async Task Should_fail_without_a_description()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => service_.CreateAsync(userId_,
                    new MovieRama.Movies.Models.CreateOptions {
                        Title = "foobar"
                    }));
        }

        [Fact]
        public async Task Should_fail_without_an_existing_user()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => service_.CreateAsync(Guid.NewGuid(),
                    new MovieRama.Movies.Models.CreateOptions {
                        Title = "foobar",
                        Description = "foobar"
                    }));
        }

        [Fact]
        public async Task Should_fail_with_a_duplicate_title()
        {
            var options = new MovieRama.Movies.Models.CreateOptions {
                Title = "foobar",
                Description = "foobar"
            };

            var movie = await service_.CreateAsync(userId_, options);

            Assert.NotNull(movie);

            await Assert.ThrowsAsync<DomainException>(
                () => service_.CreateAsync(userId_, options));
        }

        [Fact]
        public async Task Should_create_a_movie()
        {
            var user = await context_.Users.FindAsync(userId_);

            Assert.NotNull(user);

            var movie = await service_.CreateAsync(userId_,
                new MovieRama.Movies.Models.CreateOptions {
                    Title = "foobar",
                    Description = "foobar"
                });

            Assert.NotNull(movie);
            Assert.Equal(user.UserName, movie.AuditInfo.CreatedBy);
        }
    }
}
