﻿
namespace MovieRama.Tests.Services.UserRatings
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Mvc.Testing;
    using Microsoft.Extensions.DependencyInjection;

    using MovieRama.Data;
    using MovieRama.Model.Accounts;

    /// <summary>
    /// 
    /// </summary>
    public class UserRatingsUnitTests :
        IClassFixture<WebApplicationFactory<Program>>, IAsyncLifetime
    {
        /// <summary>
        /// 
        /// </summary>
        private List<Guid> userIds_;

        /// <summary>
        /// 
        /// </summary>
        private readonly ApplicationDbContext context_;

        /// <summary>
        /// 
        /// </summary>
        private readonly MovieRama.Movies.IService movies_;

        /// <summary>
        /// 
        /// </summary>
        private readonly MovieRama.Ratings.IService ratings_;

        /// <summary>
        /// 
        /// </summary>
        private readonly MovieRama.Services.UserRatings.IService userRatings_;

        /// <summary>
        /// 
        /// </summary>
        public UserRatingsUnitTests(WebApplicationFactory<Program> factory)
        {
            var scope = factory.Services
               .GetRequiredService<IServiceScopeFactory>()
               .CreateScope();

            context_ = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            movies_ = scope.ServiceProvider.GetRequiredService<MovieRama.Movies.IService>();
            ratings_ = scope.ServiceProvider.GetRequiredService<MovieRama.Ratings.IService>();
            userRatings_ = scope.ServiceProvider.GetRequiredService<MovieRama.Services.UserRatings.IService>();

            userIds_ = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task DisposeAsync()
        {
            foreach (var userId in userIds_) {
                var user = await context_.Users.FindAsync(userId);

                context_.Users.Remove(user!);
            }

            await context_.SaveChangesAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task InitializeAsync()
        {
            foreach (var userId in userIds_) {
                var user = new User {
                    Id = userId,
                    Email = $"{userId}@foobar.com",
                    UserName = $"{userId}@foobar.com",
                    ConcurrencyStamp = $"{Guid.NewGuid()}",
                    NormalizedUserName = $"{userId}@foobar.com".ToUpper()
                };

                await context_.AddAsync(user);
            }

            var movies = new List<Model.Core.Movie>();

            foreach (var userId in userIds_) {
                var movie = await movies_.CreateAsync(
                    userId, new MovieRama.Movies.Models.CreateOptions {
                        Title = $"{userId}foobar",
                        Description = $"{userId}foobar"
                    });

                movies.Add(movie);
            }

            await ratings_.RateAsync(userIds_.First(), movies.Last().Id,
                new MovieRama.Ratings.Models.RateOptions {
                    RatingScale = Constants.RatingScale.FiveStars
                });

            await ratings_.RateAsync(userIds_.Last(), movies.First().Id,
                new MovieRama.Ratings.Models.RateOptions {
                    RatingScale = Constants.RatingScale.ZeroStars
                });
        }

        [Theory]
        [InlineData(Constants.MoviesSortOptions.LikesAscending)]
        [InlineData(Constants.MoviesSortOptions.HatesAscending)]
        [InlineData(Constants.MoviesSortOptions.CreatedAscending)]
        public async Task Movies_should_be_in_ascending_order(
            string sortOrder)
        {
            var movies = await userRatings_.GetMoviesAsync(
                new MovieRama.Services.UserRatings.Models.GetMoviesOptions {
                    SortOrder = sortOrder
                });

            var firstMovie = movies.First();
            var lastMovie = movies.Last();

            switch (sortOrder) {
                case Constants.MoviesSortOptions.LikesAscending:
                    Assert.True(firstMovie.FiveStars < lastMovie.FiveStars);
                    break;
                case Constants.MoviesSortOptions.HatesAscending:
                    Assert.True(firstMovie.ZeroStars < lastMovie.ZeroStars);
                    break;
                case Constants.MoviesSortOptions.CreatedAscending:
                    Assert.True(firstMovie.TimeElapsed > lastMovie.TimeElapsed);
                    break;
            }
        }

        [Theory]
        [InlineData(null)]
        [InlineData(Constants.MoviesSortOptions.LikesDescending)]
        [InlineData(Constants.MoviesSortOptions.HatesDescending)]
        public async Task Movies_should_be_in_descending_order(
            string sortOrder)
        {
            var movies = await userRatings_.GetMoviesAsync(
                new MovieRama.Services.UserRatings.Models.GetMoviesOptions {
                    SortOrder = sortOrder
                });

            var firstMovie = movies.First();
            var lastMovie = movies.Last();

            switch (sortOrder) {
                case Constants.MoviesSortOptions.LikesDescending:
                    Assert.True(firstMovie.FiveStars > lastMovie.FiveStars);
                    break;
                case Constants.MoviesSortOptions.HatesDescending:
                    Assert.True(firstMovie.ZeroStars > lastMovie.ZeroStars);
                    break;
                default:
                    Assert.True(firstMovie.TimeElapsed < lastMovie.TimeElapsed);
                    break;
            }
        }

        [Fact]
        public async Task Movies_filtered_by_user()
        {
            var user = await context_.Users.FindAsync(userIds_.First());

            var movies = await userRatings_.GetMoviesAsync(
                new MovieRama.Services.UserRatings.Models.GetMoviesOptions {
                    Username = user!.UserName
                });

            Assert.Single(movies);
        }
    }
}
