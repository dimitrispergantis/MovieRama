﻿namespace MovieRama.Tests.Ratings
{
    using System;
    using System.Threading.Tasks;

    using Microsoft.AspNetCore.Mvc.Testing;
    using Microsoft.Extensions.DependencyInjection;

    using MovieRama.Data;
    using MovieRama.SeedWork;
    using MovieRama.Model.Accounts;

    /// <summary>
    /// 
    /// </summary>
    public class RatingsUnitTests :
        IClassFixture<WebApplicationFactory<Program>>, IAsyncLifetime
    {
        /// <summary>
        /// 
        /// </summary>
        private List<Guid> userIds_;

        /// <summary>
        /// 
        /// </summary>
        private readonly ApplicationDbContext context_;

        /// <summary>
        /// 
        /// </summary>
        private readonly MovieRama.Movies.IService movies_;

        /// <summary>
        /// 
        /// </summary>
        private readonly MovieRama.Ratings.IService ratings_;

        /// <summary>
        /// 
        /// </summary>
        /// <param name="factory"></param>
        public RatingsUnitTests(WebApplicationFactory<Program> factory)
        {
            var scope = factory.Services
                .GetRequiredService<IServiceScopeFactory>()
                .CreateScope();

            context_ = scope.ServiceProvider.GetRequiredService<ApplicationDbContext>();
            movies_ = scope.ServiceProvider.GetRequiredService<MovieRama.Movies.IService>();
            ratings_ = scope.ServiceProvider.GetRequiredService<MovieRama.Ratings.IService>();

            userIds_ = new List<Guid> {
                Guid.NewGuid(),
                Guid.NewGuid()
            };
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task InitializeAsync()
        {
            foreach (var userId in userIds_) {
                var user = new User {
                    Id = userId,
                    Email = $"{userId}@foobar.com",
                    UserName = $"{userId}@foobar.com",
                    ConcurrencyStamp = $"{Guid.NewGuid()}",
                    NormalizedUserName = $"{userId}@foobar.com".ToUpper()
                };

                await context_.AddAsync(user);
            }

            await context_.SaveChangesAsync();
        }

        /// <summary>
        /// 
        /// </summary>
        /// <returns></returns>
        public async Task DisposeAsync()
        {
            foreach (var userId in userIds_) {
                var user = await context_.Users.FindAsync(userId);

                context_.Users.Remove(user!);
            }

            await context_.SaveChangesAsync();
        }

        [Fact]
        public async Task Rating_should_fail_with_an_invalid_user()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => ratings_.RateAsync(Guid.NewGuid(), Guid.NewGuid(),
                    new MovieRama.Ratings.Models.RateOptions {
                        RatingScale = Constants.RatingScale.FiveStars
                    }));
        }

        [Fact]
        public async Task Rating_should_fail_with_an_invalid_movie()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => ratings_.RateAsync(userIds_.First(), Guid.NewGuid(),
                    new MovieRama.Ratings.Models.RateOptions {
                        RatingScale = Constants.RatingScale.FiveStars
                    }));
        }

        [Fact]
        public async Task Ratings_should_exist_on_other_users_movies()
        {
            var movie = await movies_.CreateAsync(userIds_.First(),
                new MovieRama.Movies.Models.CreateOptions {
                    Description = "foobar",
                    Title = $"{Guid.NewGuid()}foobar"
                });

            Assert.NotNull(movie);

            await Assert.ThrowsAsync<DomainException>(
                () => ratings_.RateAsync(userIds_.First(), movie.Id,
                    new MovieRama.Ratings.Models.RateOptions {
                        RatingScale = Constants.RatingScale.FiveStars
                    }));
        }

        [Theory]
        [InlineData(null)]
        [InlineData(Constants.RatingScale.ZeroStars)]
        [InlineData(Constants.RatingScale.FiveStars)]
        public async Task User_already_rated_that_movie(
            Constants.RatingScale? ratingScale)
        {
            var movie = await movies_.CreateAsync(userIds_.First(),
                new MovieRama.Movies.Models.CreateOptions {
                    Description = "foobar",
                    Title = $"{Guid.NewGuid()}foobar"
                });

            Assert.NotNull(movie);

            var rating = await ratings_.RateAsync(userIds_.Last(), movie.Id,
                new MovieRama.Ratings.Models.RateOptions {
                    RatingScale = Constants.RatingScale.FiveStars
                });

            Assert.NotNull(rating);

            await ratings_.RateAsync(userIds_.Last(), movie.Id,
                new MovieRama.Ratings.Models.RateOptions {
                    RatingScale = ratingScale
                });

            Assert.Equal(ratingScale, rating.RatingScale);
        }

        [Theory]
        [InlineData(Constants.RatingScale.ZeroStars)]
        [InlineData(Constants.RatingScale.FiveStars)]
        public async Task Movie_should_get_rated(Constants.RatingScale ratingScale)
        {
            var movie = await movies_.CreateAsync(userIds_.First(),
                new MovieRama.Movies.Models.CreateOptions {
                    Description = "foobar",
                    Title = $"{Guid.NewGuid()}foobar"
                });

            Assert.NotNull(movie);

            var rating = await ratings_.RateAsync(userIds_.Last(), movie.Id,
                new MovieRama.Ratings.Models.RateOptions {
                    RatingScale = ratingScale
                });

            Assert.NotNull(rating);
            Assert.Equal(ratingScale, rating.RatingScale);
        }

        [Fact]
        public async Task Rating_should_fail_with_null_options()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => ratings_.RateAsync(Guid.NewGuid(), Guid.NewGuid(), default));
        }

        [Fact]
        public async Task Rating_should_fail_with_invalid_user_and_movie_pair()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => ratings_.RateAsync(Guid.NewGuid(), Guid.NewGuid(),
                    new MovieRama.Ratings.Models.RateOptions {
                        RatingScale = Constants.RatingScale.ZeroStars
                    }));
        }

        [Fact]
        public async Task Rating_should_fail_with_invalid_rating_input()
        {
            await Assert.ThrowsAsync<DomainException>(
                () => ratings_.RateAsync(Guid.NewGuid(), Guid.NewGuid(),
                    new MovieRama.Ratings.Models.RateOptions {
                        RatingScale = (Constants.RatingScale)1
                    }));
        }

        [Theory]
        [InlineData(null)]
        [InlineData(Constants.RatingScale.ZeroStars)]
        [InlineData(Constants.RatingScale.FiveStars)]
        public async Task Rating_should_be_updated(
            Constants.RatingScale? ratingScale)
        {
            var movie = await movies_.CreateAsync(userIds_.First(),
                new MovieRama.Movies.Models.CreateOptions {
                    Description = "foobar",
                    Title = $"{Guid.NewGuid()}foobar"
                });

            var rating = await ratings_.RateAsync(userIds_.Last(), movie.Id,
                new MovieRama.Ratings.Models.RateOptions {
                    RatingScale = Constants.RatingScale.ZeroStars
                });

            await ratings_.RateAsync(userIds_.Last(), movie.Id,
                new MovieRama.Ratings.Models.RateOptions {
                    RatingScale = ratingScale
                });

            Assert.Equal(ratingScale, rating.RatingScale);
        }
    }
}
